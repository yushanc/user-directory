import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:9001",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
    'Access-Control-Allow-Methods': 'GET,POST,PATCH,DELETE',
  }
});
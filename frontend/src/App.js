import React, { useState } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import LoginForm from './components/loginPage';
import HubPage from './components/hub';

import PrivateRoute from './PrivateRoute';
import { AuthContext } from "./context/auth";

function App() {
  const [authTokens, setAuthTokens] = useState();

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <div className="ui container">
      <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
        <Router>
          <div>
            <PrivateRoute path="/hub" component={HubPage} />
            <Route exact path="/" component={LoginForm} />
          </div>
        </Router>
      </AuthContext.Provider>
    </div>
  )
}

// function App(props) {
//   return (
//     <Router>
//       <div>
//         <Route exact path="/" component={LoginForm} />
//         <Route path="/admin" component={HubPage} />
//       </div>
//     </Router>
//   );
// }

export default App

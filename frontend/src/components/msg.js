import React from 'react';

export const Error = () => {
  return (
    <div className="ui error message">
      <div className="header">Action Forbidden</div>
      <p>one or more request fields are missing or invalid or no field is updated</p>
    </div>
  )
}


export const Success = () => {
  return (
    <div className="ui success message">
      <div className="header">Form Completed</div>
      <p>A user is created</p>
    </div>
  )
}

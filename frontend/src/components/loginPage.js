import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import LocalApi from "../apis/local";

function LoginForm() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [email, setUserEmail] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthTokens } = useAuth();

  function postLogin() {

    let data = { email, password }

    LocalApi.post("/login", data).then(result => {
      if (result.status === 200) {
        setAuthTokens(result.data);
        setLoggedIn(true);
      } else {
        setIsError(true);
      }
    }).catch(e => {
      setIsError(true);
    });
  }

  if (isLoggedIn) {
    return <Redirect to="/hub" />;
  }

  return (
    <div className="ui middle aligned center aligned grid">
      <div className="column">
        <form className="ui large form">
          <div className="ui stacked segment">
            <div className="field">
              <div className="ui left icon input">
                <i className="user icon" />
                <input type="text" name="email" placeholder="E-mail address" onChange={e => {
                  setUserEmail(e.target.value);
                }} />
              </div>
            </div>
            <div className="field">
              <div className="ui left icon input">
                <i className="lock icon" />
                <input type="password" name="password" autoComplete="off" placeholder="Password" onChange={e => {
                  setPassword(e.target.value);
                }} />
              </div>
            </div>
            <div onClick={postLogin} className="ui fluid large teal submit button">Login</div>
          </div>
          {isError && <div className="ui message">
            <div className="header">We had some issues</div>
            <ul className="list">
              <li>The username or password provided were incorrect!</li>
            </ul>
          </div>
          }
        </form>
      </div>

    </div>
  )
}
export default LoginForm;

import React from 'react';

function userListItem({ user, onUserSelect }) {
  return (
    <tr onClick={() => onUserSelect(user)}>
      <td>{user.id}</td>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>{user.title}</td>
      <td>{user.email}</td>
    </tr>
  )
}

export default userListItem;
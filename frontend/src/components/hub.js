import React from "react";
import LocalApi from "../apis/local";
import "./../style/general.css";

import UserListView from "./userListView";
import SearchBar from "./searchBar";
import UserForm from './userForm';

class hub extends React.Component {
  state = { users: [], selectedUser: null, formEanbled: false, formTitle: "" };
  componentDidMount() {
    this.postLogin();
  };

  postLogin = () => {
    const data = { Authentication: this.props.token }

    LocalApi.get("/users", data).then(result => {
      this.setState({
        users: result.data,
        selectedUser: null,
      });
    }).catch(e => {
      console.log(`can't get users`);
    });
  };

  toggleForm = (val, title) => {
    this.setState({ formTitle: title });

    if (val === false) {
      setTimeout(() => {
        this.setState({ formEnabled: false });
      }, 500);
    } else {
      this.setState({ formEnabled: true });
    }

  }

  onUserSelect = user => {
    this.setState({
      selectedUser: user,
    });

    this.toggleForm(true);
    this.setState({ formTitle: "Update User Info" });
  };

  searchByName = name => {
    const searchWords = name.toLowerCase().replace(/\s/g, '');

    if (searchWords.length > 0) {
      const foundUsers = this.state.users.filter((user) => {
        const name = user.firstName + user.lastName;
        const combinedName = name.toLowerCase().replace(/\s/g, '')
        return combinedName === searchWords
      });
      this.setState({
        users: foundUsers,
      });
    } else {
      this.postLogin();
    }
  };

  render() {
    return (
      <div>
        {this.state.formEnabled &&
          <div className={`fixed index-2`}>
            <div className="background" onClick={() => this.toggleForm(false)}></div>
            <div className="overlay-wrapper index-3">
              <UserForm user={this.state.selectedUser} onToggle={this.toggleForm} formTitle={this.state.formTitle} />
            </div>
          </div>
        }


        <SearchBar onTermSubmit={this.searchByName} />

        <div className="ui grid">
          <div className="three column row">
            <div className="column" />
            <div className="column" />
            <div className="column right aligned">
              <button className="ui basic button" onClick={() => this.toggleForm(true, "Create User")}>
                <i className="icon user"></i>
                Add User
              </button>
            </div>
          </div>
        </div>

        <UserListView users={this.state.users} onUserSelect={this.onUserSelect} />
      </div>
    )
  }

}

export default hub;
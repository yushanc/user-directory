import React from "react";
import LocalApi from "../apis/local";
import { Error, Success } from "./msg";

class userForm extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    phone: "",
    title: "",
    createdAt: "",
    updatedAt: "",
    confirmFormSubmit: undefined,
    fieldChanged: false,
    showForm: true,
    userTitle: ""
  };

  constructor(props) {

    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    const user = props.user
    if (user) {
      this.state = { ...this.state, ...user }
    }
  };

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
      fieldChanged: true,
    });
  };

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.fieldChanged) {
      if (this.props.user) {
        // update
        LocalApi.patch(`/users/${this.props.user.id}`, this.state).then(result => {
          this.setState({
            confirmFormSubmit: "success",
            fieldChanged: false,
          });

          this.props.onToggle(false);

        }).catch(e => {
          this.setState({
            confirmFormSubmit: "error",
          });
        });
      } else {
        // create user
        LocalApi.post("/users", this.state).then(result => {
          this.setState({
            confirmFormSubmit: "success",
            fieldChanged: false,
            showForm: false,
          });

          this.props.onToggle(false);

        }).catch(e => {
          this.setState({
            confirmFormSubmit: "error"
          });
        });
      }
    } else {
      this.setState({
        confirmFormSubmit: "error",
      });
    }
  }

  render() {
    return (
      <div className="ui segment">
        <form className="ui form" onSubmit={this.handleSubmit}>
          <h4 className="ui dividing header">{this.props.formTitle}</h4>
          <div className="field">
            <div className="two fields">
              <div className="field required">
                <label>First Name</label>
                <input type="text" value={this.state.firstName} name="firstName" onChange={this.handleChange} placeholder="First Name" required />
              </div>
              <div className="field required">
                <label>Last Name</label>
                <input type="text" value={this.state.lastName} name="lastName" onChange={this.handleChange} placeholder="Last Name" required />
              </div>
              <div className="field">
                <label>Title</label>
                <input type="text" value={this.state.title} name="title" onChange={this.handleChange} placeholder="title" />
              </div>
            </div>
          </div>
          <div className="field">
            <div className="two fields">
              <div className="field required">
                <label>Email</label>
                <input type="text" value={this.state.email} name="email" onChange={this.handleChange} placeholder="email" required />
              </div>
              <div className="field required">
                <label>Phone Number</label>
                <input type="text" value={this.state.phone} name="phone" onChange={this.handleChange} placeholder="phone number" required />
              </div>
              <div className="field required">
                <label>Password</label>
                <input type="password" value={this.state.password} name="password" onChange={this.handleChange} autoComplete="off" placeholder="password" required />
              </div>
            </div>
          </div>

          <div className="ui buttons">
            <button className="ui positive button" > {this.props.user ? "Update" : "Submit"}</button>
          </div>
        </form>
        {this.state.confirmFormSubmit === "success" && <Success />}
        {this.state.confirmFormSubmit === "error" && <Error />}
      </div>


    )
  }
}
export default userForm;
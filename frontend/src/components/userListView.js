import React from "react";
import UserListItem from './userListItem';



function UserListView({ users, onUserSelect }) {

  // let startItem = 0;
  // let endItem = 6;

  //Pagination
  // const pageNumbers = [];
  // const itemPerPage = 5;

  // for (let i = 1; i <= Math.ceil(users.length / itemPerPage); i++) {
  //   pageNumbers.push(i);
  // };

  // function selectPagin(number) {
  //   startItem = endItem - itemPerPage;
  //   endItem = number * itemPerPage + (number - 1);
  //   console.log(endItem);
  // };

  const renderedList = users.length ? users.map(user => {
    return (
      <UserListItem
        href="#"
        onUserSelect={onUserSelect}
        key={user.id}
        user={user}
      />

    );

  }) : null;

  // const renderPageNumbers = pageNumbers.map(number => {
  //   return (
  //     <li
  //       className="item"
  //       key={number}
  //       href="#"
  //       onClick={() => selectPagin(number)}
  //     >{number}</li>
  //   );
  // });



  return (
    <div className="ui grid">
      <div className="sixteen wide column">
        <table className="ui selectable celled table">
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Title</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedList}
          </tbody>
        </table>
      </div>
    </div >

  )
}


export default UserListView;

# User Directory Backend
> 20/02/2020 – INTERVIEW ASSIGNMENT - VARIANT B

## Getting Started
Ensure you have the latest version of Node (tested with v10.16). In a terminal, run the following commands:
1. `npm install`
2. `npm start`

On first run, you will see a list of valid logins. **Copy this information as it will only be displayed once**. To generate a fresh database with new users and logins, delete the file at `data/db.sqlite`, and then restart the server.

The server has successfully started when you see:
> `Server started on port 9001`



<br />
<br />
<br />

# API Docs
## Authentication
All routes with the that need authentication require the following request header:
- `Authorization: Bearer <token>`

A `token` is obtained by preforming a login _(see the Login route)_.

## User Model
Most routes use the User model is some way. The following is the general structure:


| Field       | Type     | Required  | Modifiable |
| ----------- |:-------- |:---------:|:----------:|
| `id`        | Number   |           |            |
| `firstName` | String   | Y         | Y          |
| `lastName`  | String   | Y         | Y          |
| `email`     | String   | Y         | Y          |
| `password`  | String   | Y         | Y          |
| `phone`     | String   | Y         | Y          |
| `title`     | String   |           | Y          |
| `createdAt` | Date     |           |            |
| `updatedAt` | Date     |           |            |


## Errors
When an error occurs, the following response will be returned:
- **Body**:
    - `error`: String
    - `message`: String
    - `statusCode`: Number
- **Example**:
```json
{
  "error": "NotAuthenticated",
  "message": "Invalid credentials",
  "statusCode": 401
}
```

## Routes
### Login
- **URL**: `/login`
- **Method**: `POST`
#### Request:
- **Body**: 
    - `email`: String _(required)_
    - `password`: String _(required)_
- **Example**:
```json
{
  "email": "test@example.com",
  "password": "test1234"
}
```

#### Response:
- **Code**: `200 Ok`
- **Body**:
    - `token`: String
- **Example**:
```json
{
  "token": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
}
```

#### Error Response:
- **Condition**: If the `email` or `password` fields are invalid, or their combination is wrong.
- **Code**: `401 NotAuthenticated`
- **Body**: _see Errors section above_

---

### User List
- **URL**: `/users`
- **Method**: `GET`
- **Authentication**: required
#### Request:
- **Body**: none

#### Response:
- **Code**: `200 Ok`
- **Body**: Array of `User` objects

---

### Get User
- **URL**: `/users/:id`
- **Method**: `GET`
- **Authentication**: required
#### Request:
- **URL Params**:
    - `id`: ID of the user to fetch
- **Body**: none

#### Response:
- **Code**: `200 Ok`
- **Body**: `User` object

#### Error Response:
- **Condition**: If no user was found with the specified ID
- **Code**: `404 NotFound`
- **Body**: _see Errors section above_

---

### Create User
- **URL**: `/users`
- **Method**: `POST`
- **Authentication**: required
#### Request:
- **Body**: 
  - `firstName`: String _(required)_
  - `lastName`: String _(required)_
  - `email`: String _(required)_
  - `password`: String _(required)_
  - `phone`: String _(required)_
  - `title`: String
- **Example**:
```json
{
  "firstName": "John",
  "lastName": "Doe",
  "email": "jdoe@example.com",
  "password": "test1234",
  "phone": "123-234-3456",
  "title": "CFO"
}
```
#### Response:
- **Code**: `200 Ok`
- **Body**: `User` object

#### Error Response:
- **Condition**: If one or more request fields are missing or invalid
- **Code**: `400 BadRequest`
- **Body**: _see Errors section above_

---

### Patch User
- **URL**: `/users/:id`
- **Method**: `PATCH`
- **Authentication**: required
#### Request:
- **URL Params**:
    - `id`: ID of the user to patch
- **Body**: At least one of the following
  - `firstName`: String
  - `lastName`: String
  - `email`: String
  - `password`: String
  - `phone`: String
  - `title`: String
- **Example**:
```json
{
  "phone": "123-234-3456 ext. 1",
  "title": "CEO"
}
```
#### Response:
- **Code**: `200 Ok`
- **Body**: `User` object

#### Error Response:
- **Conditions**: 
    - If no user was found with the specified ID
    - If one or more request fields are missing or invalid
- **Codes**: 
    - `400 BadRequest`
    - `404 NotFound`
- **Body**: _see Errors section above_


---

### Delete User
- **URL**: `/users/:id`
- **Method**: `DELETE`
- **Authentication**: required
#### Request:
- **URL Params**:
    - `id`: ID of the user to delete
- **Body**: none

#### Response:
- **Code**: `200 Ok`
- **Body**: `User` object

#### Error Response:
- **Condition**: If no user was found with the specified ID
- **Code**: `404 NotFound`
- **Body**: _see Errors section above_

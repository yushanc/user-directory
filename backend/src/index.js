const App = require('./app')
const PORT = 9001

async function main () {
  const app = await App()
  const port = PORT
  app.listen(port, () => console.log(`Server started on port ${port}`))
}

process.on('unhandledRejection', (reason, p) =>
  console.error('Unhandled Rejection at Promise', p, reason)
)

main()
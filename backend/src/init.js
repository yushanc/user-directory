const faker = require('faker')
const auth = require('./auth')

module.exports = function init (app) {
  const promises = []
  const User = app.get('User')

  promises.push(User.findAll()
    .then(users => {
      if (users.length) return

      const newUsers = []
      for (let i = 0; i < 25; i++) {
        const firstName = faker.name.firstName()
        const lastName = faker.name.lastName()
        newUsers.push({
          firstName,
          lastName,
          email: firstName + lastName + '@example.com',
          password: faker.internet.password(),
          phone: faker.phone.phoneNumberFormat(),
          title: faker.name.title(),
          createdAt: faker.date.between('2015-01-01', '2020-01-01')
        })
      }

      // Save a few logins before password hasing for display
      const selectLogins = newUsers.slice(0, 5).map(user => ({
        email: user.email,
        password: user.password
      }))

      // Hash passwords
      return Promise.all(newUsers.map(user => {
        return auth.hashPassword(user.password)
          .then(hash => {
            user.password = hash
            return user
          })
      }))
        .then(() => User.bulkCreate(newUsers))
        .then(() => {
          console.log('Fresh users created! Here are 5 logins:')
          console.table(selectLogins)
        })
    }))

  return Promise.all(promises)
}
